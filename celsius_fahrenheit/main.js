/**
 *
 * @param {Number} celsius La température en celsius
 * @returns {Number} La température en fahrenheit
 */
function celsius_vers_fahrenheit(celsius) {
  // TODO
  return 0;
}

// Appel au clique du button
function action() {
  const text = document.querySelector("#text");
  const celsius = Number(text.value);
  const fahrenheit = celsius_vers_fahrenheit(celsius);
  update_ui(celsius, fahrenheit);
}

function update_ui(celsius, fahrenheit) {
  const p = document.querySelector("#result");
  p.innerText = `${celsius} deg celsius correspond à ${fahrenheit} deg Fahrenheit`;
}
