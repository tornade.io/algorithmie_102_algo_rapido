/**
 *
 * @param {String} mot
 * @param {Number} decalage
 * @returns
 */
function chiffrement(mot, decalage) {
  // TODO
  return "";
}

// Chiffre le message
function cipher() {
  const decalage = Number(document.querySelector("#decalage").value);
  const mot = document.querySelector("#cipher").value;
  const cipher = chiffrement(mot, decalage);
  update_ui(cipher);
}

function update_ui(cipher) {
  const p = document.querySelector("#result");
  p.innerText = `La phrase chiffré : ${cipher}`;
}
