/**
 *
 * @param {String} texte
 * @returns Un objet avec une clef consonne et une clef voyelle
 */
function compter(texte) {
  // TODO
  return {};
}

// Appel la function au clique du bouton
function action() {
  const text = document.querySelector("#text");
  const compteur = compter(text.value);
  update_ui(compteur);
}

function update_ui(compteur) {
  const p = document.querySelector("#result");
  p.innerText = `Consonne: ${compteur["consonne"]} ; Voyelle: ${compteur["voyelle"]}`;
}
