/**
 *
 * @param {Numeber} table_of 5, 3, etc...
 * @returns An array of result of the tab
 */
function gen_table(table_of) {
  // TODO
  return [];
}

const MAX_TABLE = 10;
// Appel au clique du button
function action() {
  const input = Number(document.querySelector("#text").value);
  const table = gen_table(input);
  update_ui(input, table);
}

function update_ui(mul, table) {
  const container = document.querySelector("#result");
  for (let i = 0; i < MAX_TABLE; i++) {
    const p = document.createElement("p");
    p.innerText = `${i + 1} * ${mul} = ${table[i]}`;
    container.appendChild(p);
  }
}
