/**
 *
 * @param {String} mot le mot a tester
 * @returns {Boolean}
 */
function est_un_palindrome(mot) {
  // TODO
  return false;
}

// Appel au clique du button
function action() {
  const text = document.querySelector("#text");
  const est_palindrome = est_un_palindrome(text.value);
  update_ui(est_palindrome);
}

function update_ui(est_palindrome) {
  const p = document.querySelector("#result");
  p.innerText = `Le mot est un palindrome ? ${est_palindrome ? "Oui" : "Non"}`;
}
