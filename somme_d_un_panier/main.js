// Faites la sommes du paniers dans cette fonction
function calculer_somme() {
  // TODO
  return 0;
}

function retrouver_panier() {
  const container = document.querySelector("#basket");
  const panier = [];
  container.childNodes.forEach((child) => {
    panier.push(child.getAttribute("price"));
  });
  return panier;
}

function refresh_panier(panier) {
  const container = document.querySelector("#basket");
  container.innerHTML = "";
  for (let i = 0; i < panier.length; i++) {
    const p = document.createElement("p");
    p.innerText = `Élément ${i + 1} : ${panier[i]} euros`;
    p.setAttribute("price", panier[i]);
    container.appendChild(p);
  }
}

const PRIX_BASE = 100;
function ajouter_dans_panier() {
  const prix = Math.random() * PRIX_BASE;

  const panier = retrouver_panier();
  panier.push(prix);
  refresh_panier(panier);
  update_ui(panier);
}

function update_ui(panier) {
  const somme = calculer_somme(panier);
  const total_container = document.querySelector("#total");
  total_container.innerText = `Prix Total : ${somme} euros`;
}
