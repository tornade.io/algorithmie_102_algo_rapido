/**
 * Retourner le nombre le plus grand dans cette fonction
 * @param {String} texte le texte dans le textarea
 * @returns le mot le plus long du texte
 */
function le_mot_le_plus_grand(texte) {
  console.log(texte);
  // TODO
  return "TODO";
}

// Appel la function trouve le plus long avec le text
function action() {
  const text = document.querySelector("#text");
  const plus_grand = le_mot_le_plus_grand(text.value);
  update_ui(plus_grand);
}

function update_ui(plus_grand) {
  const p = document.querySelector("#result");
  p.innerText = `${plus_grand}`;
}
