// Retourner le nombre le plus grand dans cette fonction
function plus_grand() {
  // TODO
  return 0;
}

// Récupère l'ensemble des élèments généré
function recuperer_nombres() {
  const container = document.querySelector("#nombres");
  const nombres = [];
  container.childNodes.forEach((child) => {
    nombres.push(Number(child.getAttribute("rnd_round")));
  });
  return nombres;
}

// Des constantes pour la génération
// N'hésites pas à jouer avec pour comprendre le code

const borne_max = 100;
const borne_minimale = 10;

// Générer un nombre et l'ajoute dans le DOM.
function gen_element() {
  const rnd = Math.random() * borne_max + borne_minimale;
  const rnd_round = Math.round(rnd);

  const container = document.querySelector("#nombres");
  const p = document.createElement("p");
  p.innerText = `${rnd_round}`;
  p.setAttribute("rnd_round", rnd_round);
  container.appendChild(p);
  update_ui();
}

function update_ui() {
  const p = document.querySelector("#plusgrand");
  p.innerText = `Le nombre le plus grand est ${plus_grand()}`;
}
